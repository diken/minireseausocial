// Imports
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var models = require('.bin/models');



// Routes
module.exports = {
    register: function(req, res) {
        // Params
        var email = req.body.email;
        var username = req.body.username;
        var password = req.body.password;
        var bio = req.body.bio;
        var isAdmin = req.body.isAdmin;
        var image = req.body.image;

        // Test params
        if (email == null || username == null || password == null) {
            return res.status(400).json({ 'error': 'missing parameters'});          
        }
        if (image != null) {
            var saveBase64Image = require('./base64ToImage').saveBase64Image(image);
        }
        // verif params

        models.User.findOne({
            attributes: ['email'],
            where: { email: email }
        }).then(function(userFound){
            if (!userFound){
                
                bcrypt.hash(password, 5, function(err, bcryptedPassword) {
                    var newUser = models.User.create({
                        email: email,
                        username: username,
                        password: bcryptedPassword,
                        bio: bio,
                        isAdmin: isAdmin,
                        image: (image != null) ? saveBase64Image : ''
                    }).then(function(newUser){
                        return res.status(201).json({ 'userId': newUser.id });
                    }).catch(function(err){
                        return res.status(500).json({ 'error': 'Cannot add user '});
                    });
                });
            } else {
                return res.status(409).json({ 'error': 'user already exist !'});
            }
        }).catch(function(err){
            return res.status(500).json({ 'error': 'unable to verify user'});
        });
    },

    login: function(req, res) {
        //
    }
}

