// Imports
var crypto = require('crypto');
var fs = require('fs');


// format : 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAZABkAAD/4Q3zaHR0cDovL25zLmFkb2JlLmN...';
 exports.saveBase64Image = (function(base64Data) {
 var imageName = '';
 try
 {
    
    var matches = base64Data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    if (matches.length !== 3) 
    {
        return new Error('Invalid input string');
    }
     // Regular expression for image type:
     // This regular image extracts the "jpeg" from "image/jpeg"
     var imageTypeRegularExpression = /\/(.*?)$/;      

     // Generate random string
     var seed = crypto.randomBytes(20);
     var uniqueSHA1String = crypto.createHash('sha1').update(seed).digest('hex');

     var imageBuffer = new Buffer(matches[2], 'base64');
     var imageType = matches[1];
     var chemin = './images/';

     var uniqueRandomImageName = 'image-' + uniqueSHA1String;
     // This variable is actually an array which has 5 values,
     // The [1] value is the real image extension
     var imageTypeDetected = imageType.match(imageTypeRegularExpression);

     var userUploadedImagePath = chemin + uniqueRandomImageName +'.'+ imageTypeDetected[1];
     
     // Save decoded binary image to disk
     try
     {
         fs.writeFile(userUploadedImagePath, imageBuffer, (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
            imageName = uniqueRandomImageName +'.'+ imageTypeDetected[1];
          }
        );
        
        
     }
     catch(error)
     {
         console.log('ERROR:', error);
     }
     
     
 }
 catch(error)
 {
     console.log('ERROR:', error);
 }   
 return imageName;  
 });

